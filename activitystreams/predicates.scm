;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (activitystreams predicates)
  #:use-module (activitystreams ontology)
  #:use-module (ice-9 match)
  #:use-module (jsonld json)
  #:use-module (web uri)
  #:export (uri? float? mime-type? date? string-or-lang-string? duration?
            langtag? link-relation? non-negative-integer? time? date-time?))

(define (uri? s)
  (and (string? s) (string->uri s)))

(define (float? s)
  (or (number? s)
      (and (string? s) (string->number s))))

(define (mime-type? s)
  (string? s))

(define (date? s)
  (string? s))

(define (string-or-lang-string? s)
  (or (string? s) (as-string? s)))

(define (duration? s)
  (string? s))

(define (langtag? s)
  (string? s))

(define (link-relation? s)
  (string? s))

(define (non-negative-integer? s)
  (and (number? s) (>= s 0)))

(define (time? s)
  (string? s))

(define (date-time? s)
  (string? s))
