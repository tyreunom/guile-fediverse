;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 
;;;;
;;;;
;;;; Note that most of this file is a direct translation of the activitystreams
;;;; vocabulary specification (esp. comments in type and property) which
;;;; is under these terms:
;;;;
;;;;    Copyright © 2017 Activity Streams Working Group, IBM & W3C® (MIT,
;;;;    ERCIM, Keio, Beihang). W3C liability, trademark and permissive
;;;;    document license rules apply.
;;;;
;;;; You should have received a copy of the Permissive Document License along
;;;; with this library; if not, that document license is accessible online at:
;;;; https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
;;;;
;;;; The origin document used to develop this can be found at:
;;;; https://www.w3.org/TR/activitystreams-vocabulary

(define-module (activitystreams vocabulary)
  #:use-module (activitystreams ontology)
  #:use-module (activitystreams predicates)
  #:use-module (ice-9 match)
  #:use-module (jsonld json)
  #:use-module (web uri)
  #:export (activitystreams-ontology))

;; Core types

(define-public Object
  (build-as-type
    "Object"
    #:comment
    "Describes an object of any kind.  The Object type serves as the base type
for most of the other kinds of objects defined in the Activity Vocabulary,
including other Core types such as Activity, IntransitiveActivity, Collection
and OrderedCollection."))

(define-public Link
  (build-as-type
    "Link"
    #:comment
    "A Link is an indirect, qualified reference to a resource identified by a
URL.  The fundamental model for links is established by [RFC5988].  Many of the
properties defined by the Activity Vocabulary allow values that are either
instances of Object or Link.  When a Link is used, it establishes a qualified
relation connecting the subject (the containing object) to the resource
identified by the href.  Properties of the Link are properties of the
reference as opposed to properties of the resource."))

(define-public Activity
  (build-as-type
    "Activity"
    #:subclass-of (list Object)
    #:comment
    "An Activity is a subtype of Object that describes some form of action that
may happen, is currently happening, or has already happened.  The Activity type
itself serves as an abstract base type for all types of activities.  It is
important to note that the Activity type itself does not carry any specific
semantics about the kind of action being taken."))

(define-public IntransitiveActivity
  (build-as-type
    "IntrasitiveActivity"
    #:subclass-of (list Activity)
    #:comment
    "Instances of IntransitiveActivity are a subtype of Activity representing
intransitive actions.  The object property is therefore inappropriate for
these activities."))

(define-public Collection
  (build-as-type
    "Collection"
    #:subclass-of (list Object)
    #:comment
    "A Collection is a subtype of Object that represents ordered or unordered
sets of Object or Link instances.  Refer to the Activity Streams 2.0 Core
specification for a complete description of the Collection type."))

(define-public OrderedCollection
  (build-as-type
    "OrderedCollection"
    #:subclass-of (list Collection)
    #:comment
    "A subtype of Collection in which members of the logical collection are
assumed to always be strictly ordered."))

(define-public CollectionPage
  (build-as-type
    "CollectionPage"
    #:subclass-of (list Collection)
    #:comment
    "Used to represent distinct subsets of items from a Collection.  Refer to
the Activity Streams 2.0 Core for a complete description of the CollectionPage
object."))

(define-public OrderedCollectionPage
  (build-as-type
    "OrderedCollectionPage"
    #:subclass-of (list OrderedCollection CollectionPage)
    #:comment
    "Used to represent ordered subsets of items from an OrderedCollection.
Refer to the Activity Streams 2.0 Core for a complete description of the
OrderedCollectionPage object."))

(define as-core-types (list Object Link Activity IntransitiveActivity
                            Collection OrderedCollection CollectionPage
                            OrderedCollectionPage))

;; Extended Types
;; Activity Types

(define-public Accept
  (build-as-type
    "Accept"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor accepts the object.  The target property can be
used in certain circumstances to indicate the context into which the object has
been accepted."))

(define-public TentativeAccept
  (build-as-type
    "TentativeAccept"
    #:subclass-of (list Accept)
    #:comment
    "A specialization of Accept indicating that the acceptance is tentative."))

(define-public Add
  (build-as-type
    "Add"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor has added the object to the target.  If the target
property is not explicitly specified, the target would need to be determined
implicitly by context.  The origin can be used to identify the context from
which the object originated."))

(define-public Arrive
  (build-as-type
    "Arrive"
    #:subclass-of (list IntransitiveActivity)
    #:comment
    "An IntransitiveActivity that indicates that the actor has arrived at the
location.  The origin can be used to identify the context from which the actor
originated.  The target typically has no defined meaning."))

(define-public Create
  (build-as-type
    "Create"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor has created the object."))

(define-public Delete
  (build-as-type
    "Delete"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor has deleted the object.  If specified, the origin
indicates the context from which the object was deleted."))

(define-public Follow
  (build-as-type
    "Follow"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor is ``following'' the object.  Following is defined
in the sense typically used within Social systems in which the actor is
interested in any activity performed by or on the object.  The target and
origin typically have no defined meaning."))

(define-public Ignore
  (build-as-type
    "Ignore"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor is ignoring the object.  The target and origin
typically have no defined meaning."))

(define-public Join
  (build-as-type
    "Join"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor has joined the object.  The target and origin
typically have no defined meaning."))

(define-public Leave
  (build-as-type
    "Leave"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor has left the object.  The target and origin
typically have no meaning."))

(define-public Like
  (build-as-type
    "Like"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor likes, recommends or endorses the object.  The
target and origin typically have no defined meaning."))

(define-public Offer
  (build-as-type
    "Offer"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor is offering the object.  If specified, the target
indicates the entity to which the object is being offered."))

(define-public Invite
  (build-as-type
    "Invite"
    #:subclass-of (list Offer)
    #:comment
    "A specialization of Offer in which the actor is extending an invitation
for the object to the target."))

(define-public Reject
  (build-as-type
    "Reject"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor is rejecting the object.  The target and origin
typically have no defined meaning."))

(define-public TentativeReject
  (build-as-type
    "TentativeReject"
    #:subclass-of (list Reject)
    #:comment
    "A specialization of Reject in which the rejection is considered tentative."))

(define-public Remove
  (build-as-type
    "Remove"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor is removing the object.  If specified, the
origin indicates the context from which the object is being removed."))

(define-public Undo
  (build-as-type
    "Undo"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor is undoing the object.  In most cases, the object
will be an Activity describing some previously performed action (for instance,
a person may have previously ``liked'' an article but, for whatever reason,
might choose to undo that like at some later point in time).  The target and
origin typically have no defined meaning."))

(define-public Update
  (build-as-type
    "Update"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor has updated the object.  Note, however, that this
vocabulary does not define a mechanism for describing the actual set of
modifications made to object.  The target and origin typically have no defined
meaning."))

(define-public View
  (build-as-type
    "View"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor has viewed the object."))

(define-public Listen
  (build-as-type
    "Listen"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor has listened to the object."))

(define-public Read
  (build-as-type
    "Read"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor has read the object."))

(define-public Move
  (build-as-type
    "Move"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor has moved object from origin to target.  If the
origin or target are not specified, either can be determined by context."))

(define-public Travel
  (build-as-type
    "Travel"
    #:subclass-of (list IntransitiveActivity)
    #:comment
    "Indicates that the actor is traveling to target from origin.  Travel is
an IntransitiveObject whose actor specifies the direct object.  If the target
or origin are not specified, either can be determined by context."))

(define-public Announce
  (build-as-type
    "Announce"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor is calling the target's attention the object.  The
origin typically has no defined meaning."))

(define-public Block
  (build-as-type
    "Block"
    #:subclass-of (list Ignore)
    #:comment
    "Indicates that the actor is blocking the object.  Blocking is a stronger
form of Ignore.  The typical use is to support social systems that allow one
user to block activities or content of other users.  The target and origin
typically have no defined meaning."))

(define-public Flag
  (build-as-type
    "Flag"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor is ``flagging'' the object.  Flagging is defined
in the sense common to many social platforms as reporting content as being
inappropriate for any number of reasons."))

(define-public Dislike
  (build-as-type
    "Dislike"
    #:subclass-of (list Activity)
    #:comment
    "Indicates that the actor dislikes the object."))

(define-public Question
  (build-as-type
    "Question"
    #:subclass-of (list IntransitiveActivity)
    #:comment
    "Represents a question being asked.  Question objects are an extension of
IntransitiveActivity.  That is, the Question object is an Activity, but the
direct object is the question itself and therefore it would not contain an
object property.  Either of the anyOf and oneOf properties MAY be used to
express possible answers, but a Question object MUST NOT have both properties."))

(define as-activity-types
  (list Accept Add Announce Arrive Block Create Delete
        Dislike Flag Follow Ignore Invite Join Leave
        Like Listen Move Offer Question Reject Read
        Remove TentativeReject TentativeAccept Travel Undo
        Update View))

;; Actor Types
(define-public Application
  (build-as-type
    "Application"
    #:subclass-of (list Object)
    #:comment
    "Describes a software application."))

(define-public Group
  (build-as-type
    "Group"
    #:subclass-of (list Object)
    #:comment
    "Represents a formal or informal collective of Actors."))

(define-public Organisation
  (build-as-type
    "Organisation"
    #:subclass-of (list Object)
    #:comment
    "Represents an organization."))

(define-public Person
  (build-as-type
    "Person"
    #:subclass-of (list Object)
    #:comment
    "Represents an individual person."))

(define-public Service
  (build-as-type
    "Service"
    #:subclass-of (list Object)
    #:comment
    "Represents a service of any kind."))

(define as-actor-types
  (list Application Group Organisation Person Service))

;; 

(define-public Relationship
  (build-as-type
    "Relationship"
    #:subclass-of (list Object)
    #:comment
    "Describes a relationship between two individuals.  The subject and object
properties are used to identify the connected individuals.  See 5.2 Representing
Relationships Between Entities for additional information."))

(define-public Article
  (build-as-type
    "Article"
    #:subclass-of (list Object)
    #:comment
    "Represents any kind of multi-paragraph written work."))

(define-public Document
  (build-as-type
    "Document"
    #:subclass-of (list Object)
    #:comment
    "Represents a document of any kind."))

(define-public Audio
  (build-as-type
    "Audio"
    #:subclass-of (list Document)
    #:comment
    "Represents an audio document of any kind."))

(define-public Image
  (build-as-type
    "Image"
    #:subclass-of (list Document)
    #:comment
    "An image document of any kind."))

(define-public Video
  (build-as-type
    "Video"
    #:subclass-of (list Document)
    #:comment
    "Represents a video document of any kind."))

(define-public Note
  (build-as-type
    "Note"
    #:subclass-of (list Object)
    #:comment
    "Represents a short written work typically less than a single paragraph in
length."))

(define-public Page
  (build-as-type
    "Page"
    #:subclass-of (list Document)
    #:comment
    "Represents a Web Page."))

(define-public Event
  (build-as-type
    "Event"
    #:subclass-of (list Object)
    #:comment
    "Represents any kind of event."))

(define-public Place
  (build-as-type
    "Place"
    #:subclass-of (list Object)
    #:comment
    "Represents a logical or physical location.  See 5.3 Representing Places
for additional information."))

(define-public Mention
  (build-as-type
    "Mention"
    #:subclass-of (list Link)
    #:comment
    "A specialized Link that represents an @mention."))

(define-public Profile
  (build-as-type
    "Profile"
    #:subclass-of (list Object)
    #:comment
    "A Profile is a content object that describes another Object, typically
used to describe Actor Type objects.  The describes property is used to
reference the object being described by the profile"))

(define-public Tombstone
  (build-as-type
    "Tombstone"
    #:subclass-of (list Object)
    #:comment
    "A Tombstone represents a content object that has been deleted.  It can be
used in Collections to signify that there used to be an object at this
position, but it has been deleted."))

(define as-object-and-link-types
  (list Article Audio Document Event Image Note Page
        Place Profile Relationship Tombstone Video Mention))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                Properties                                 ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-public id
  (build-as-property
    "id" (list Object Link) uri?
    #:uri "@id"
    #:comment "Provides the globally unique identifier for an Object or Link."
    #:functional? #t))

(define-public type
  (build-as-property
    "type" (list Object Link) uri?
    #:uri "@type"
    #:comment "Identifies the Object or Link type.  Multiple values may be
specified."))

(define-public attributedTo
  (build-as-property
    "attributedTo" (list Object Link) (list Object Link)
    #:comment
    "Identifies one or more entities to which this object is attributed.  The
attributed entities might not be Actors.  For instance, an object might be
attributed to the completion of another activity."))

(define-public actor
  (build-as-property
    "actor" (list Object Link) (list Object Link)
    #:subproperty-of (list attributedTo)
    #:comment
    "Describes one or more entities that either performed or are expected to
perform the activity.  Any single activity can have multiple actors.  The actor
MAY be specified using an indirect Link."))

(define-public attachment
  (build-as-property
    "attachment" Object (list Object Link)
    #:comment
    "Identifies a resource attached or related to an object that potentially
requires special handling.  The intent is to provide a model that is at least
semantically similar to attachments in email."))

(define-public audience
  (build-as-property
    "audience" Object (list Object Link)
    #:comment
    "Identifies one or more entities that represent the total population of
entities for which the object can considered to be relevant."))

(define-public bcc
  (build-as-property
    "bcc" Object (list Object Link)
    #:comment
    "Identifies one or more Objects that are part of the private secondary
audience of this Object."))

(define-public bto
  (build-as-property
    "bto" Object (list Object Link)
    #:comment
    "Identifies an Object that is part of the private primary audience of this
Object."))

(define-public cc
  (build-as-property
    "cc" Object (list Object Link)
    #:comment
    "Identifies an Object that is part of the public secondary audience of
this Object."))

(define-public context
  (build-as-property
    "context" Object (list Object Link)
    #:comment
    "Identifies the context within which the object exists or an activity was
performed.  The notion of ``context'' used is intentionally vague.  The
intended function is to serve as a means of grouping objects and activities
that share a common originating context or purpose.  An example could be all
activities relating to a common project or event."))

(define-public current
  (build-as-property
    "current" Collection (list CollectionPage Link)
    #:functional? #t
    #:comment
    "In a paged Collection, indicates the page that contains the most recently
updated member items."))

(define-public first
  (build-as-property
    "first" Collection (list CollectionPage Link)
    #:functional? #t
    #:comment
    "In a paged Collection, indicates the furthest preceeding page of items in
the collection."))

(define-public generator
  (build-as-property
    "generator" Object (list Object Link)
    #:comment
    "Identifies the entity (e.g. an application) that generated the object."))

(define-public icon
  (build-as-property
    "icon" Object (list Image Link)
    #:comment
    "Indicates an entity that describes an icon for this object.  The image
should have an aspect ratio of one (horizontal) to one (vertical) and should
be suitable for presentation at a small size."))

(define-public image
  (build-as-property
    "image" Object (list Image Link)
    #:comment
    "Indicates an entity that describes an image for this object.  Unlike the
icon property, there are no aspect ratio or display size limitations assumed."))

(define-public inReplyTo
  (build-as-property
    "inReplyTo" Object (list Object Link)
    #:comment
    "Indicates one or more entities for which this object is considered a
response."))

(define-public instrument
  (build-as-property
    "instrument" Activity (list Object Link)
    #:comment
    "Identifies one or more objects used (or to be used) in the completion of
an Activity."))

(define-public last
  (build-as-property
    "last" Collection (list CollectionPage Link)
    #:functional? #t
    #:comment
    ""))

(define-public location
  (build-as-property
    "location" Object (list Object Link)
    #:comment
    "Indicates one or more physical or logical locations associated with the
object."))

(define-public items
  (build-as-property
    "items" Collection (list Object Link)
    #:comment
    "Identifies the items contained in a collection. The items might be ordered
or unordered."))

(define-public oneOf
  (build-as-property
    "oneOf" Question (list Object Link)
    #:comment
    "Identifies an exclusive option for a Question.  Use of oneOf implies that
the Question can have only a single answer.  To indicate that a Question can
have multiple answers, use anyOf."))

(define-public anyOf
  (build-as-property
    "anyOf" Question (list Object Link)
    #:comment
    "Identifies an inclusive option for a Question.  Use of anyOf implies that
the Question can have multiple answers.  To indicate that a Question can have
only one answer, use oneOf."))

(define-public closed
  (build-as-property
    "closed" Question (list Object Link date? boolean?)
    #:comment
    "Indicates that a question has been closed, and answers are no longer
accepted."))

(define-public origin
  (build-as-property
    "origin" Activity (list Object Link)
    #:comment
    "Describes an indirect object of the activity from which the activity is
directed.  The precise meaning of the origin is the object of the English
preposition ``from''.  For instance, in the activity ``John moved an item to
List B from List A'', the origin of the activity is ``List A''."))

(define-public next
  (build-as-property
    "next" CollectionPage (list CollectionPage Link)
    #:functional? #t
    #:comment
    "In a paged Collection, indicates the next page of items."))

(define-public object
  (build-as-property
    "object" (list Activity Relationship) (list Object Link)
    #:comment
    "When used within an Activity, describes the direct object of the activity.
For instance, in the activity ``John added a movie to his wishlist'', the
object of the activity is the movie added.  When used within a Relationship
describes the entity to which the subject is related."))

(define-public prev
  (build-as-property
    "prev" CollectionPage (list CollectionPage Link)
    #:functional? #t
    #:comment
    "In a paged Collection, identifies the previous page of items."))

(define-public preview
  (build-as-property
    "preview" (list Object Link) (list Object Link)
    #:comment
    "Identifies an entity that provides a preview of this object."))

(define-public result
  (build-as-property
    "result" Activity (list Object Link)
    #:comment
    "Describes the result of the activity.  For instance, if a particular
action results in the creation of a new resource, the result property can be
used to describe that new resource."))

(define-public replies
  (build-as-property
    "replise" Object Collection
    #:functional? #t
    #:comment
    "Identifies a Collection containing objects considered to be responses to
this object."))

(define-public tag
  (build-as-property
    "tag" Object (list Object Link)
    #:comment
    "One or more ``tags'' that have been associated with an object.  A tag can
be any kind of Object.  The key difference between attachment and tag is that
the former implies association by inclusion, while the latter implies
associated by reference."))

(define-public target
  (build-as-property
    "target" Activity (list Object Link)
    #:comment
    "Describes the indirect object, or target, of the activity.  The precise
meaning of the target is largely dependent on the type of action being
described but will often be the object of the English preposition ``to''.  For
instance, in the activity ``John added a movie to his wishlist'', the target
of the activity is John's wishlist. An activity can have more than one target."))

(define-public to
  (build-as-property
    "to" Object (list Object Link)
    #:comment
    "Identifies an entity considered to be part of the public primary audience
of an Object."))

(define-public url
  (build-as-property
    "url" Object (list uri? Link)
    #:comment
    "Identifies one or more links to representations of the object."))

(define-public accuracy
  (build-as-property
    "accuracy" Place float?
    #:comment
    "Indicates the accuracy of position coordinates on a Place objects.
Expressed in properties of percentage.  e.g. ``94.0'' means ``94.0% accurate''."))

(define-public altitude
  (build-as-property
    "altitude" Object float?
    #:functional? #t
    #:comment
    "Indicates the altitude of a place.  The measurement units is indicated
using the units property.  If units is not specified, the default is assumed
to be ``m'' indicating meters."))

(define-public content
  (build-as-property
    "content" Object string-or-lang-string?
    #:comment
    "The content or textual representation of the Object encoded as a JSON
string.  By default, the value of content is HTML.  The mediaType property can
be used in the object to indicate a different content type.  The content MAY
be expressed using multiple language-tagged values."))

(define-public name
  (build-as-property
    "name" (list Object Link) string-or-lang-string?
    #:comment
    "A simple, human-readable, plain-text name for the object.  HTML markup
MUST NOT be included.  The name MAY be expressed using multiple language-tagged
values."))

(define-public duration
  (build-as-property
    "duration" Object duration?
    #:functional? #t
    #:comment
    "When the object describes a time-bound resource, such as an audio or video,
a meeting, etc, the duration property indicates the object's approximate
duration.  The value MUST be expressed as an xsd:duration as defined by
[xmlschema11-2], section 3.3.6 (e.g. a period of 5 seconds is represented as
``PT5S'')."))

(define-public height
  (build-as-property
    "height" Link non-negative-integer?
    #:functional? #t
    #:comment
    "On a Link, specifies a hint as to the rendering height in device-independent
pixels of the linked resource."))

(define-public href
  (build-as-property
    "href" Link uri?
    #:functional? #t
    #:comment
    "The target resource pointed to by a Link."))

(define-public hreflang
  (build-as-property
    "hreflang" Link langtag?
    #:functional? #t
    #:comment
    "Hints as to the language used by the target resource.  Value MUST be a
[BCP47] Language-Tag."))

(define-public partOf
  (build-as-property
    "partOf" CollectionPage (list Collection Link)
    #:functional? #t
    #:comment
    "Identifies the Collection to which a CollectionPage objects items belong."))

(define-public latitude
  (build-as-property
    "latitude" Place float?
    #:functional? #t
    #:comment
    "The latitude of a place."))

(define-public longitude
  (build-as-property
    "longitude" Place float?
    #:functional? #t
    #:comment
    "The longitude of a place."))

(define-public mediaType
  (build-as-property
    "mediaType" (list Link Object) mime-type?
    #:functional? #t
    #:comment
    "When used on a Link, identifies the MIME media type of the referenced
resource.  When used on an Object, identifies the MIME media type of the value
of the content property.  If not specified, the content property is assumed to
contain text/html content."))

(define-public endTime
  (build-as-property
    "endTime" Object date?
    #:functional? #t
    #:comment
    "The date and time describing the actual or expected ending time of the
object.  When used with an Activity object, for instance, the endTime property
specifies the moment the activity concluded or is expected to conclude."))

(define-public published
  (build-as-property
    "published" Object date?
    #:functional? #t
    #:comment
    "The date and time at which the object was published."))

(define-public startTime
  (build-as-property
    "startTime" Object date?
    #:functional? #t
    #:comment
    "The date and time describing the actual or expected starting time of the
object.  When used with an Activity object, for instance, the startTime
property specifies the moment the activity began or is scheduled to begin."))

(define-public radius
  (build-as-property
    "radius" Place float?
    #:functional? #t
    #:comment
    "The radius from the given latitude and longitude for a Place.  The units
is expressed by the units property.  If units is not specified, the default is
assumed to be ``m'' indicating ``meters''."))

(define-public rel
  (build-as-property
    "rel" Link link-relation?
    #:comment
    "A link relation associated with a Link.  The value MUST conform to both the
[HTML5] and [RFC5988] ``link relation'' definitions.  In the [HTML5], any
string not containing the ``space'' U+0020, ``tab'' (U+0009), ``LF'' (U+000A),
``FF'' (U+000C), ``CR'' (U+000D) or ``,'' (U+002C) characters can be used as a
valid link relation."))

(define-public startIndex
  (build-as-property
    "startIndex" OrderedCollectionPage non-negative-integer?
    #:functional? #t
    #:comment
    "A non-negative integer value identifying the relative position within the
logical view of a strictly ordered collection."))

(define-public summary
  (build-as-property
    "summary" Object string-or-lang-string?
    #:comment
    "A natural language summarization of the object encoded as HTML.  Multiple
language tagged summaries MAY be provided."))

(define-public totalItems
  (build-as-property
    "totalItems" Collection non-negative-integer?
    #:functional? #t
    #:comment
    "A non-negative integer specifying the total number of objects contained by
the logical view of the collection.  This number might not reflect the actual
number of items serialized within the Collection object instance."))

(define-public units
  (build-as-property
    "units" Place (list uri? (lambda (s) (member s '("cm" "feet" "inches" "km"
                                                     "m" "miles"))))
    #:functional? #t
    #:comment
    "Specifies the measurement units for the radius and altitude properties on
a Place object.  If not specified, the default is assumed to be ``m'' for
``meters''."))

(define-public updated
  (build-as-property
    "updated" Object date?
    #:functional? #t
    #:comment
    "The date and time at which the object was updated."))

(define-public width
  (build-as-property
    "width" Link non-negative-integer?
    #:functional? #t
    #:comment
    "On a Link, specifies a hint as to the rendering width in device-independent
pixels of the linked resource."))

(define-public subject
  (build-as-property
    "subject" Relationship (list Object Link)
    #:functional? #t
    #:comment
    "On a Relationship object, the subject property identifies one of the
connected individuals.  For instance, for a Relationship object describing
``John is related to Sally'', subject would refer to John."))

(define-public relationship
  (build-as-property
    "relationship" Relationship Object
    #:comment
    "On a Relationship object, the relationship property identifies the kind of
relationship that exists between subject and object."))

(define-public describes
  (build-as-property
    "describes" Profile Object
    #:functional? #t
    #:comment
    "On a Profile object, the describes property identifies the object described
by the Profile."))

(define-public formerType
  (build-as-property
    "formerType" Tombstone Object
    #:comment
    "On a Tombstone object, the formerType property identifies the type of the
object that was deleted."))

(define-public deleted
  (build-as-property
    "deleted" Tombstone date?
    #:functional? #t
    #:comment
    "On a Tombstone object, the deleted property is a timestamp for when the
object was deleted."))

(define as-properties
  (list actor attachment attributedTo audience bcc bto
        cc context current first generator icon id
        image inReplyTo instrument last location items
        oneOf anyOf closed origin next object prev
        preview result replies tag target to type
        url accuracy altitude content name duration
        height href hreflang partOf latitude longitude
        mediaType endTime published startTime radius
        rel startIndex summary totalItems units updated
        width subject relationship describes formerType
        deleted))

(define activitystreams-ontology
  (make-ontology
    '("https://www.w3.org/ns/activitystreams")
    (append as-core-types as-activity-types as-actor-types as-object-and-link-types)
    as-properties))

