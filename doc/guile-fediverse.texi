\input texinfo
@setfilename guile-fediverse.info
@documentencoding UTF-8
@settitle guile-fediverse

@include version.texi 

@copying
Copyright @copyright{} 2020 Julien Lepiller

@quotation
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A
copy of the license is included in the section entitled ``GNU Free
Documentation License''.
@end quotation
@end copying

@titlepage
@end titlepage

@contents

@node Top
@top guile-fediverse

This document describes guile-fediverse version @value{VERSION}, a guile
implementation of some algorithms and specifications used in the Fediverse.

@menu
* Introduction::           What is the Fediverse?
* API Reference::          Description of the library interface.

@detailmenu
--- The Detailed Node Listing ---

API Reference

* ActivityStreams API::    Encoding and Decoding Activities.
* Webfinger API::          Finding resource locations.

@end detailmenu
@end menu

@node Introduction
@chapter Introduction

The Fediverse is a collection of applications and servers that provide a social
media platform and similar services.  They individually work as a platform,
with users (identities) and services and collectivelly form a bigger social
network, the Fedivere, by the use of different federating protocols.  Some
of them are implemented in this library.

If you are new to the concept of the Fediverse, you should try out one of the
great servers that provide service for Mastodon (the most well-known server),
Pleroma (a micro-blogging platform), Peertube (a video sharing platform), and
many more!  With one account on one of these platforms, you can interact with
any account on any of these other platforms.  Imagine commenting a Peertube
video from Mastodon and sharing it with a friend on Pleroma :)

@node API Reference
@chapter API Reference

@node ActivityStreams API
@section ActivityStreams API

TODO

@node Webfinger API
@section Webfinger API

Webfinger is a protocol that allows you to query resource location at a
well-known end-point.  The protocol is described in detail in
@uref{RFC 7033, https://tools.ietf.org/html/rfc7033}.

This library provides a small interface to use this protocol in the
@code{(webfinger webfinger)} module.

First of all, webfinger answers are in the form of a @dfn{JRD} record, meaning
JSON Resource Descriptor.  This record is encoded in the @code{<jrd-record>}
data type:

@deffn {Datatype} <jrd-record>

@table @asis
@item @code{subject}
The resource that was requested and for which the answer is given.  This is
a URI, as a string.

@item @code{aliases}
A list of zero or more URI as strings, that identify the same entity.

@item @code{properties}
A list of key-value pairs (an alist) whose keys and values are strings.  This
represents a set of properties on the subject that the webfinger server wants
to give you.

@item @code{links}
A list of links that correspond to the location of the requested identity.  These
links are objects of type @code{<link-record>}.

@end table
@end deffn

@deffn {Datatype} <link-record>

@table @asis
@item @code{rel}
A string that is either a URI or a registered relation type, as defined at
@url{http://www.iana.org/assignments/link-relations/}.

@item @code{type}
A string that represents the MIME/Type of the content accessible through the
link.

@item @code{href}
A string that represents the URI at which you can find of the requested
resource.

@item @code{titles}
An association list where keys are strings that represent a language name and
value represents the title for the content of the link in that language.

@item @code{properties}
A list of key-value pairs (an alist) whose keys and values are strings.  This
represents a set of properties on the link that the webfinger server wants
to give you.

@end table
@end deffn

The library provides two procedures to retrieve data from a webfinger server.
A low-level procedure that can query anything and a higher-level procedure to
specifically query actor objects to a Fediverse server.

@deffn {Scheme Procedure} webfinger-query @var{server} @var{resource} @
  [@var{rel}]
Queries a webfinger @var{server} for @var{resource}, which must be a URI,
represented as a string.  Returns a @code{jrd-record} object.

@end deffn

@deffn {Scheme Procedure} find-actor-object @var{user} @var{server}
Queries a webfinger @var{server} for the actor object of @@@var{user}@@@var{server}.
Note that @var{server} should be a string that represents the server name, but
has no scheme.  The procedure returns a string that corresponds to the best
link returned by the webfinger server (determined by the type when available).

This queries the resource ``acct:@var{user}@@@var{server}'' to the server.
@end deffn

@bye
