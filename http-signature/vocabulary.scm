;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (http-signature vocabulary)
  #:use-module (activitystreams ontology)
  #:use-module (activitystreams predicates)
  #:use-module (ice-9 match)
  #:use-module (jsonld json)
  #:use-module (web uri)
  #:export (security-ontology))

(define-public vocab "https://w3id.org/security#")

(define* (make-sec-type label #:key (uri (string-append vocab label))
                        (comment "") (subclass-of '()))
  (build-as-type label #:uri uri #:comment comment #:subclass-of subclass-of))

(define* (make-sec-property label domain range
                           #:key (uri (string-append vocab label))
                           (functional? #f) (subproperty-of '()) (comment ""))
  (build-as-property label domain range #:uri uri #:functional? functional?
                    #:subproperty-of subproperty-of #:comment comment))

;; Classes

(define-public Digest
  (make-sec-type
    "Digest"
    #:comment
    "This class represents a message digest that may be used for data integrity
verification.  The digest algorithm used will determine the cryptographic
properties of the digest."))

(define-public EncryptedMessage
  (make-sec-type
    "EncryptedMessage"
    #:comment
    "A class of messages that are obfuscated in some cryptographic manner.
These messages are incredibly difficult to decrypt without the proper
decryption key."))

(define-public Signature
  (make-sec-type
    "Signature"
    #:comment
    "This class represents a digital signature on serialized data.  It is an
abstract class and should not be used other than for Semantic Web reasoning
purposes, such as by a reasoning agent."))

(define-public GraphSignature2012
  (make-sec-type
    "GraphSignature2012"
    #:subclass-of (list Signature)
    #:comment
    "A graph signature is used for digital signatures on RDF graphs.  The
default canonicalization mechanism is specified in the RDF Graph normalization
specification, which effectively deterministically names all unnamed nodes.
The default signature mechanism uses a SHA-256 digest and RSA to perform the
digital signature."))

(define-public LinkedDataSignature2015
  (make-sec-type
    "LinkedDataSignature2015"
    #:subclass-of (list Signature)
    #:comment
    "A Linked Data signature is used for digital signatures on RDF Datasets.
The default canonicalization mechanism is specified in the RDF Dataset
Normalization specification, which effectively deterministically names all
unnamed nodes.  The default signature mechanism uses a SHA-256 digest and RSA
to perform the digital signature.  This signature uses a algorithm for
producing the data that it signs and verifies that is different from other
Linked Data signatures."))

(define-public LinkedDataSignature2016
  (make-sec-type
    "LinkedDataSignature2016"
    #:subclass-of (list Signature)
    #:comment
    "A Linked Data signature is used for digital signatures on RDF Datasets.
The default canonicalization mechanism is specified in the RDF Dataset
Normalization specification, which effectively deterministically names all
unnamed nodes. The default signature mechanism uses a SHA-256 digest and RSA
to perform the digital signature."))

(define-public Key
  (make-sec-type
    "Key"
    #:comment
    "This class represents a cryptographic key that may be used for encryption,
decryption, or digitally signing data."))

(define-public sec-types (list Digest EncryptedMessage Signature
                        GraphSignature2012 LinkedDataSignature2015
                        LinkedDataSignature2016 Key))

(define-public authenticationTag
  (make-sec-property
    "authenticationTag" "EncryptedMessage" string?
    #:comment
    "Not specified"))

(define-public creator
  (make-sec-property
    "creator" "Signature" uri?
    #:comment
    "Not specified"))

(define-public cipherAlgorithm
  (make-sec-property
    "cipherAlgorithm" "EncryptedMessage" string?
    #:comment
    "The cipher algorithm describes the mechanism used to encrypt a message.
It is typically a string expressing the cipher suite, the strength of the
cipher, and a block cipher mode."))

(define-public cipherData
  (make-sec-property
    "cipherData" "EncryptedMessage" string?
    #:comment
    "Cipher data an opaque blob of information that is used to specify an
encrypted message."))

(define-public digestAlgorithm
  (make-sec-property
    "digestAlgorithm" "Digest" string?
    #:comment
    "The digest algorithm is used to specify the cryptographic function to use
when generating the data to be digitally signed.  Typically, data that is to be
signed goes through three steps: 1) canonicalization, 2) digest, and
3) signature.  This property is used to specify the algorithm that should be
used for step #2.  A signature class typically specifies a default digest
method, so this property is typically used to specify information for a
signature algorithm."))

(define-public digestValue
  (make-sec-property
    "digestValue" "Digest" string?
    #:comment
    "The digest value is used to express the output of the digest algorithm
expressed in Base-16 (hexadecimal) format."))

(define-public cipherKey
  (make-sec-property
    "cipherKey" "EncryptedMessage" string?
    #:comment
    "A cipher key is a symmetric key that is used to encrypt or decrypt a
piece of information.  The key itself may be expressed in clear text or
encrypted."))

(define-public expires
  (make-sec-property
    "expires" "" string?
    #:comment
    "The expiration time is typically associated with a Key and specifies when
the validity of the key will expire.  It is considered a best practice to only
create keys that have very definite expiration periods.  This period is
typically set to between six months and two years.  An digital signature
created using an expired key MUST be marked as invalid by any software
attempting to verify the signature."))

(define-public initializationVector
  (make-sec-property
    "initializationVector" "EncryptedMessage" string?
    #:comment
    "The initialization vector (IV) is a byte stream that is typically used to
initialize certain block cipher encryption schemes.  For a receiving
application to be able to decrypt a message, it must know the decryption key
and the initialization vector.  The value is typically base-64 encoded."))

(define-public nonce
  (make-sec-property
    "nonce" "" string?
    #:comment
    "This property is used in conjunction with the input to the signature
hashing function in order to protect against replay attacks.  Typically,
receivers need to track all nonce values used within a certain time period
in order to ensure that an attacker cannot merely re-send a compromised
packet in order to execute a privileged request."))

(define-public canonicalizationAlgorithm
  (make-sec-property
    "canonicalizationAlgorithm" "" (list uri? string?)
    #:comment
    "The canonicalization algorithm is used to transform the input data into a
form that can be passed to a cryptographic digest method.  The digest is then
digitally signed using a digital signature algorithm.  Canonicalization ensures
that a piece of software that is generating a digital signature is able to do
so on the same set of information in a deterministic manner."))

(define-public owner
  (make-sec-property
    "owner" "Key" uri?
    #:comment
    "An owner is an entity that claims control over a particular resource.
Note that ownership is best validated as a two-way relationship where the
owner claims ownership over a particular resource, and the resource clearly
identifies its owner."))

(define-public password
  (make-sec-property
    "password" "" string?
    #:comment
    "A secret that is used to generate a key that can be used to encrypt or
decrypt message.  It is typically a string value."))

(define-public privateKeyPem
  (make-sec-property
    "privateKeyPem" "Key" string?
    #:comment
    "A private key PEM property is used to specify the PEM-encoded version of
the private key.  This encoding is compatible with almost every Secure Sockets
Layer library implementation and typically plugs directly into functions
intializing private keys."))

(define-public publicKey
  (make-sec-property
    "publicKey" "EncryptedMessage" uri?
    #:comment
    "A public key property is used to specify a URL that contains information
about a public key."))

(define-public publicKeyPem
  (make-sec-property
    "publicKeyPem" "Key" string?
    #:comment
    "A public key PEM property is used to specify the PEM-encoded version of
the public key.  This encoding is compatible with almost every Secure Sockets
Layer library implementation and typically plugs directly into functions
intializing public keys."))

(define-public publicKeyService
  (make-sec-property
    "publicKeyService" "" string?
    #:comment
    "The publicKeyService property is used to express the REST URL that provides
public key management services as defined by the Web Key specification."))

(define-public revoked
  (make-sec-property
    "revoked" "" date?
    #:comment
    "The revocation time is typically associated with a Key that has been
marked as invalid as of the date and time associated with the property.  Key
revocations are often used when a key is compromised, such as the theft of the
private key, or during the course of best-practice key rotation schedules."))

(define-public signature
  (make-sec-property
    ;; XXX: not correct, there is no defined domain
    "signature" "Object" "Signature"
    #:comment
    "The signature property is used to associate a signature with a graph of
information.  The signature property is typically not included in the
canonicalized graph that is then digested, and digitally signed."))

(define-public signatureValue
  (make-sec-property
    "signatureValue" "Signature" string?
    #:comment
    "The signature value is used to express the output of the signature
algorithm expressed in base-64 format."))

(define-public signatureAlgorithm
  (make-sec-property
    "signatureAlgorithm" "Signature" string?
    #:comment
    "The signature algorithm is used to specify the cryptographic signature
function to use when digitally signing the digest data.  Typically, text to be
signed goes through three steps: 1) canonicalization, 2) digest, and
3) signature.  This property is used to specify the algorithm that should be
used for step #3.  A signature class typically specifies a default signature
algorithm, so this property rarely needs to be used in practice when
specifying digital signatures."))

(define sec-properties
  (list cipherAlgorithm cipherData digestAlgorithm digestValue
        cipherKey expires initializationVector nonce
        canonicalizationAlgorithm owner password
        privateKeyPem publicKey publicKeyPem publicKeyService
        revoked signature signatureValue signatureAlgorithm))

(define security-ontology
  (make-ontology '("https://w3id.org/security/v1") sec-types sec-properties))
