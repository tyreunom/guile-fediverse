;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 
;;;;
;;;;
;;;; Note that most of this file is a direct translation of the activitystreams
;;;; vocabulary specification (esp. comments in type and property) which
;;;; is under these terms:
;;;;
;;;;    Copyright © 2017 Activity Streams Working Group, IBM & W3C® (MIT,
;;;;    ERCIM, Keio, Beihang). W3C liability, trademark and permissive
;;;;    document license rules apply.
;;;;
;;;; You should have received a copy of the Permissive Document License along
;;;; with this library; if not, that document license is accessible online at:
;;;; https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
;;;;
;;;; The origin document used to develop this can be found at:
;;;; https://www.w3.org/TR/activitystreams-vocabulary

(define-module (activitypub vocabulary)
  #:use-module ((activitystreams vocabulary) #:prefix as:)
  #:use-module (activitystreams ontology)
  #:use-module (activitystreams predicates)
  #:use-module (ice-9 match)
  #:use-module (jsonld json)
  #:use-module (web uri)
  #:export (activitypub-ontology))

(define-public source
  (build-as-property
    "source" as:Object json-object?
    #:comment
    "The source property is intended to convey some sort of source from which
the content markup was derived, as a form of provenance, or to support future
editing by clients.  In general, clients do the conversion from source to
content, not the other way around."))

(define-public content
  (build-as-property
    "content" source string-or-lang-string?))

(define-public mediaType
  (build-as-property
    "mediaType" source mime-type?))

(define-public inbox
  (build-as-property
    "inbox" as:Object as:OrderedCollection
    #:comment "The inbox stream contains all activities received by the actor.
The server should filter content according to the requester's permission.  In
general, the owner of an inbox is likely to be able to access all of their
inbox contents.  Depending on access control, some other content may be
public, whereas other content may require authentication for non-owner users,
if they can access the inbox at all."))

(define-public outbox
  (build-as-property
    "outbox" as:Object as:OrderedCollection
    #:comment
    "The outbox stream contains activities the user has published, subject to
the ability of the requestor to retrieve the activity (that is, the contents of
the outbox are filtered by the permissions of the person reading it).  If a
user submits a request without Authorization the server should respond with all
of the Public posts.  This could potentially be all relevant objects published
by the user, though the number of available items is left to the discretion of
those implementing and deploying the server."))

(define-public following
  (build-as-property
    "following" as:Object as:Collection
    #:comment
    "This is a list of everybody that the actor has followed, added as a side
effect.  The following collection MUST be either an OrderedCollection or a
Collection and MAY be filtered on privileges of an authenticated user or as
appropriate when no authentication is given."))

(define-public followers
  (build-as-property
    "followers" as:Object as:Collection
    #:comment
    "This is a list of everyone who has sent a Follow activity for the actor,
added as a side effect.  This is where one would find a list of all the actors
that are following the actor.  The followers collection MUST be either an
OrderedCollection or a Collection and MAY be filtered on privileges of an
authenticated user or as appropriate when no authentication is given."))

(define-public liked
  (build-as-property
    "liked" as:Object as:Collection
    #:comment
    "This is a list of every object from all of the actor's Like activities,
added as a side effect.  The liked collection MUST be either an OrderedCollection
or a Collection and MAY be filtered on privileges of an authenticated user or
as appropriate when no authentication is given."))

(define-public streams
  (build-as-property
    "streams" as:Object as:Collection
    #:comment
    "A list of supplementary Collections which may be of interest."))

(define-public preferredUsername
  (build-as-property
    "preferredUsername" as:Object string?
    #:comment
    "A short username which may be used to refer to the actor, with no
uniqueness guarantees."))

(define-public endpoints
  (build-as-property
    "endpoints" as:Object json-object?
    #:comment
    "A json object which maps additional (typically server/domain-wide)
endpoints which may be useful either for this actor or someone referencing this
actor.  This mapping may be nested inside the actor document as the value or
may be a link to a JSON-LD document with these properties."))

(define-public proxyUrl
  (build-as-property
    "proxyUrl" endpoints uri?
    #:comment
    "Endpoint URI so this actor's clients may access remote ActivityStreams
objects which require authentication to access.  To use this endpoint, the
client posts an x-www-form-urlencoded id parameter with the value being the id
of the requested ActivityStreams object."))

(define-public oauthAuthorizationEndpoint
  (build-as-property
    "oauthAuthorizationEndpoint" endpoints uri?
    #:comment
    "If OAuth 2.0 bearer tokens are being used for authenticating client to
server interactions, this endpoint specifies a URI at which a
browser-authenticated user may obtain a new authorization grant."))

(define-public oauthTokenEndpoint
  (build-as-property
    "oauthTokenEndpoint" endpoints uri?
    #:comment
    "If OAuth 2.0 bearer tokens are being used for authenticating client to
server interactions, this endpoint specifies a URI at which a client may
acquire an access token."))

(define-public provideClientKey
  (build-as-property
    "provideClientKey" endpoints uri?
    #:comment
    "If Linked Data Signatures and HTTP Signatures are being used for
authentication and authorization, this endpoint specifies a URI at which
browser-authenticated users may authorize a client's public key for client to
server interactions."))

(define-public signClientKey
  (build-as-property
    "signClientKey" endpoints uri?
    #:comment
    "If Linked Data Signatures and HTTP Signatures are being used for
authentication and authorization, this endpoint specifies a URI at which a
client key may be signed by the actor's key for a time window to act on behalf
of the actor in interacting with foreign servers."))

(define-public sharedInbox
  (build-as-property
    "sharedInbox" endpoints uri?
    #:comment
    "An optional endpoint used for wide delivery of publicly addressed
activities and activities sent to followers.  sharedInbox endpoints SHOULD also
be publicly readable OrderedCollection objects containing objects addressed to
the Public special collection.  Reading from the sharedInbox endpoint MUST NOT
present objects which are not addressed to the Public endpoint."))

(define activitypub-ontology
  (merge-ontologies as:activitystreams-ontology
                    (make-ontology
                      '()
                      '()
                      (list source content mediaType inbox outbox following
                            followers liked streams preferredUsername
                            endpoints proxyUrl oauthAuthorizationEndpoint
                            oauthTokenEndpoint provideClientKey signClientKey
                            sharedInbox))))
