;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(use-modules
  ((guix licenses) #:prefix license:)
  (guix build-system gnu)
  (guix download)
  (guix git-download)
  (guix packages)
  (guix utils)
  (gnu packages autotools)
  (gnu packages gnupg)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages texinfo)
  (gnu packages tls))

(define my-guile-json
  (package
    (inherit guile-json-4)
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://github.com/aconchillo/guile-json")
                     (commit "2e9512b43e31bb697efd359509e986321a903f69")))
              (file-name (git-file-name "guile-json"
                                        (string-append
                                          (package-version guile-json-4)
                                          "-2e9512b")))
              (sha256
               (base32
                "1al7pa9726fsj4hki0zqzgknn3v2kn3d01wp6fmkdc3cwpyamanc"))))
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("pkg-config" ,pkg-config)
       ,@(package-native-inputs guile-json-4)))))

(package
  (name "guile-fediverse")
  (version "0.1.0")
  (source
    (origin
      (method git-fetch)
      (uri (git-reference
             (url "https://framagit.org/tyreunom/guile-fediverse")
             (commit version)))
      (file-name (git-file-name name version))
      (sha256
       (base32
        "00l03j8ajkd1a7sg1zycbpdaz71mscrncw7rwjzqk2ia6j04rwxm"))))
  (build-system gnu-build-system)
  (arguments
   `(#:make-flags '("SCHEMA_ORG_FILE=schema.org/schema.jsonld")))
  (inputs
   `(("guile" ,guile-3.0)
     ("guile-json" ,my-guile-json)
     ("guile-jsonld" ,guile-jsonld)
     ("guile-gcrypt" ,guile-gcrypt)))
  (native-inputs
   `(("automake" ,automake)
     ("autoconf" ,autoconf)
     ("pkg-config" ,pkg-config)
     ("texinfo" ,texinfo)))
  (home-page "https://framagit.org/tyreunom/guile-fediverse")
  (synopsis "Implementation of multiple Fediverse algorithms")
  (description "")
  (license license:lgpl3+))
